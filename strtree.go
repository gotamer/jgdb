package jgdb

import (
	"code.google.com/p/eaburns/strtree"
)

var files strtree.T

func Build() {
	for i := range RootFiles() {
		files.Insert(i)
	}
}

func GetAll() (list []string) {
	files.Iterate(func(s string) {
		list = append(list, s)
	})
	return list
}

func Count() (c int) {
	c = files.Len()
	return
}
