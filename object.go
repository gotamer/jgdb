// ### Object (Multi file persistent database)
//
// Object type keeps each record in a separate file. 
//    * The object type will keep track of these records with an index file
//    * Optionally with a strtree implementation for fast in memory access.
package jgdb

import (
	"encoding/json"
	"errors"
	"gotamer/e"
	"os"
	"path/filepath"
	"strings"
	"sync"
)

type Object struct {
	mutex   sync.RWMutex
	baseDir string
	Key     string
	Data    Struct
	Folders []string
}

// NewObject returns a new data store.
func NewObject(baseDir string) *Object {
	d := &Object{
		mutex:   sync.RWMutex{},
		baseDir: baseDir,
	}
	return d
}

// Add or replace a json file.
func (o *Object) Save() error {
	o.mutex.Lock()
	defer o.mutex.Unlock()

	if len(o.Key) == 0 {
		o.newfile()
	}

	f, err := os.OpenFile(o.filepath(), os.O_WRONLY|os.O_CREATE|os.O_TRUNC, FilePerm)
	if err != nil {
		return err
	}
	defer f.Close()
	j, err := json.MarshalIndent(&o.Data, "", TAB)
	if err != nil {
		return err
	}
	_, err = f.Write(j)
	return err
}

// newfile creates the folders and 
// returns the absolute path to a new file.
func (o *Object) newfile() (err error) {

	// Fail safe counter / way too may files in one folder / database is full ;)
	// @todo Find a more eligant way to solve this
	var count uint

	if len(o.baseDir) < 1 {
		err = errors.New("An Object Database needs a baseDir")
		return err
	}

	path := o.location()

	if err = os.MkdirAll(path, DirPerm); err == nil {

	NewFileGenerator:
		o.Key = srand()
		path = o.filepath()

		if _, err = os.Stat(path); err != nil && count < 5 {
			count++
			goto NewFileGenerator
		}
	}
	return
}

// location returns the absolute path on the filesystem
// where the data for the given key will be stored.
func (o *Object) location() (path string) {
	path = o.basepath()
	if len(o.Folders) != 0 {
		path = path + ps + strings.Join(o.Folders, ps)
	}
	return
}

// basepath returns the absolute path on the filesystem
// where the database will be stored.
func (o *Object) basepath() (path string) {
	path, err := filepath.Abs(ROOT + ps + "object" + ps + o.baseDir)
	e.Fail(err)
	return
}

// filepath returns the absolute path on the filesystem
// where the data for the given key will be stored.
func (o *Object) filepath() string {
	return o.location() + ps + o.Key + ".json"
}
