// ### JGDB stands for Json Git Database
// 
// http://robotamer.com/html/GoTamer/JGDB.html
//
package jgdb

import (
	"math/rand"
	"os"
	"path/filepath"
	"time"
)

const (
	characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	EOL        = "\n"
	TAB        = "\t"
)

var (
	Debug      uint8
	ps         string      = string(os.PathSeparator)
	ROOT       string      = os.Getenv("GOPATH") + ps + "var" + ps + "jgdb"
	rootLength uint8       = uint8(len(ROOT))
	FilePerm   os.FileMode = 0664
	DirPerm    os.FileMode = 0775
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

// Struct is an abstract type for your struct.
type Struct interface{}

// RootFiles returns a channel that will yield every file accessible in undefined order.
func RootFiles() <-chan string {
	c := make(chan string)
	go func() {
		filepath.Walk(ROOT, walker(c))
		close(c)
	}()
	return c
}

// walker returns a function, which satisfies the filepath.WalkFunc interface.
// It sends every file found in path exapt index.json down the channel c.
func walker(c chan string) func(path string, info os.FileInfo, err error) error {
	return func(path string, info os.FileInfo, err error) error {
		if err == nil && !info.IsDir() && info.Name() != "index.json" {
			c <- path[rootLength:]
		}
		return nil
	}
}

// srand generates a random string
func srand() string {
	var length int
	length = 10
	buffer := make([]byte, length)
	for i := 0; i < length; i++ {
		buffer[i] = characters[rand.Intn(len(characters)-1)]
	}
	return string(buffer)
}
