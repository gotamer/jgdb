package jgdb

import "testing"
import "fmt"

type Page struct {
	Title string
	Body  string
	User  string
}

func TestObject(t *testing.T) {
	d := new(Page)
	d.Title = "My Page Title"
	d.Body = "My Page Body, and some more text"
	d.User = "RoboTamer"

	db := NewObject("tmp")
	db.Data = d
	db.Folders = append(db.Folders, "test")

	err := db.Save()
	if err != nil {
		println(err.Error())
	}
}

func TestIndexAdd(t *testing.T) {
	d := new(Page)
	d.Title = "My Page Title"
	d.Body = "My Page Body, and some more text 1"
	d.User = "RoboTamer"

	db := NewObject("tmp")
	db.Data = d
	db.Folders = append(db.Folders, "test")

	err := db.Save()
	if err != nil {
		println(err.Error())
	}

	path := db.filepath()

	Add2Index("tmp", db.Key, path)

	k := Get(db.Key)
	fmt.Printf("Now %s is a %T\n", k, k)
}

func TestIndexIter(t *testing.T) {
	for i := range Iter() {
		println(i)
		k := Get(i)
		fmt.Printf("Key %s links to %s\n", i, k)
	}
}

func TestIndexKeys(t *testing.T) {
	for i := range RootFiles() {
		println(i)
		k := Get(i)
		fmt.Printf("Key %s links to %s\n", i, k)
	}
}

type User struct {
	Key  uint
	Name string
}

func TestList(t *testing.T) {
	users, err := NewList("users")
	if err != nil {
		println(err.Error())
		t.FailNow()
	}
	if err = users.Load(); err != nil {
		println(err.Error())
		t.FailNow()
	}
	/*
			var addnew = func() func() {
				users.count++
				return func() {
					m := new(User)
					m.Key = users.count * users.count
					m.Name = fmt.Sprintf("MName %d", users.count)
					users.Add(m)
				}
			}()

			addnew()

		user := new(User)
		user.Key = 25
		u := users.Find(user)
		fmt.Println(u)
	*/
}
