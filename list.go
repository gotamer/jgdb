// ### List (Single file persistent database)
// 
// The List type uses a single file as backend with each record placed in one line.
// 
//    * The user/dev creates a struct to his/her needs  
//    * The database inherits that struct via an Interface  
//    * The database converts that struct to include the map id using jsonListStruct  
//    * jsonListStruct struct is used to Marshal the data in to a json file  
package jgdb

import (
	"bufio"
	"encoding/json"
	//"fmt"
	"io"
	"os"
	"path/filepath"
	"syscall"
)

//var jsonMap map[string]interface{}

type jsonListStruct struct {
	Id     int
	Struct interface{}
}

type List struct {
	Tuple *Tuple
	name  string
}

// NewList returns a new list data store.
func NewList(name string) (o *List, err error) {
	o = new(List)
	o.name = name
	o.Tuple = NewTuple()
	err = os.MkdirAll(filepath.Dir(o.filepath()), DirPerm)
	return
}

func (o *List) Add(data interface{}) (e error) {
	o.Tuple.mutex.Lock()
	defer o.Tuple.mutex.Unlock()
	k := o.Tuple.Len() + 1
	if e = o.add(k, data); e == nil {
		o.Tuple.T[k] = data
	}
	return
}

// add only adds the entry to the file
func (o *List) add(key int, data interface{}) (e error) {
	j := new(jsonListStruct)
	j.Id = key
	j.Struct = data
	e = addToListFile(o.filepath(), *j)
	return e
}

// Del will remove the entry with the given index key k.  
func (o *List) Del(k int) {
	o.Tuple.mutex.Lock()
	defer o.Tuple.mutex.Unlock()
	o.Tuple.Purge(k)
	o.ReGenFile()
}

// filepath returns the absolute path on the filesystem
// where the data for the given list will be stored.
func (l *List) filepath() string {
	path, err := filepath.Abs(ROOT + ps + "list" + ps + l.name + ".json")
	if err != nil {
		println(err.Error())
		panic("Probably bad database name")
	}
	return path
}

// Save a new object to the json file.
func addToListFile(f string, j jsonListStruct) (err error) {
	if js, err := json.Marshal(&j); err == nil {
		if f, err := os.OpenFile(f, os.O_WRONLY|os.O_CREATE|os.O_APPEND, FilePerm); err == nil {
			defer f.Close()
			if _, err = f.Write(js); err == nil {
				_, err = f.WriteString(EOL)
			}
		}
	}
	return
}

// Load will load the json file line by line in to memory
func (o *List) Load() (err error) {
	f, err := os.Open(o.filepath())
	if err == nil {
		defer f.Close()
		d := json.NewDecoder(bufio.NewReader(f))
		for {
			var jo jsonListStruct
			if err = d.Decode(&jo); err == io.EOF {
				err = nil
				break
			} else if err == nil {
				o.Tuple.T[jo.Id] = jo.Struct
			}
		}
	}
	return
}

// Copying original file to tmp folder and recreating db file
func (o *List) ReGenFile() (e error) {
	if e = os.Rename(o.filepath(), os.TempDir()+o.name); e == nil {
		for k, v := range o.Tuple.T {
			if e = o.add(v); e != nil {
				os.Rename(os.TempDir()+o.name, o.filepath())
				syscall.Unlink(os.TempDir() + o.name)
				return e
			}
		}
		syscall.Unlink(os.TempDir() + o.name) // clean up 
	}
	return
}
